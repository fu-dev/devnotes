module.exports = {
  title: 'DevNotes',
  description: 'This is a Zeit Now 2.0 example',
  themeConfig: {
    navbar: true,
    sidebar: ['/', ['/guide/puppeteer', 'Puppeteer']]
  }
};
