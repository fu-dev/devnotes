---
tags: ['puppeteer', 'jest']
---

# Puppeteer

## Get and check classNames with jest

Immer wieder beim E2E Test gebraucht - classNames checken

```JavaScript
const CLASSES = await page.$eval('.element', el => el.className);
await expect(CLASSES).toContain('disabled');
```
